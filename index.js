module.exports = {
  WeloinSms: require('./weloinSms'),
  WeloinSmsOtp: require('./weloinSmsOtp'),
  WeloinSmsSimple: require('./weloinSmsSimple'),
  WeloinOtpValidate: require('./weloinOtpValidate'),
}

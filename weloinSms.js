const axios = require('axios')

/**
 * WeloinSms class provides functionality to send SMS and OTP using the Weloin SMS service.
 */
class WeloinSms {
  static _instance = null

  /**
   * Returns a singleton instance of WeloinSms.
   * @returns {WeloinSms} Singleton instance of WeloinSms.
   */
  static instance() {
    if (this._instance === null) {
      this._instance = new WeloinSms({ baseUrl: null, accessKey: null })
    }
    return this._instance
  }

  /**
   * Initializes a new instance of WeloinSms.
   * @param {Object} options - Options for initializing WeloinSms.
   * @param {string} options.baseUrl - Base URL of the Weloin SMS service.
   * @param {string} options.accessKey - Access key for authentication.
   */
  constructor({ baseUrl, accessKey }) {
    this.baseUrl = baseUrl ?? process.env.WELOINSMS_BASE_URL
    this.accessKey = accessKey ?? process.env.WELOINSMS_ACCESS_KEY
  }

  /**
   * Sends an SMS or OTP depending on the type of SMS object provided.
   * @param {WeloinSmsSimple|WeloinSmsOtp} sms - SMS object to send.
   * @returns {Promise<Object>} A promise that resolves with the response data from the API.
   * @throws {Error} If the provided SMS object is invalid.
   */
  async send(sms) {
    if (sms.constructor.name === 'WeloinSmsSimple')
      return await this.sendSms(sms)

    if (sms.constructor.name === 'WeloinOtpSimple')
      return await this.sendOtp(sms)

    throw new Error('Invalid sms envelop!!')
  }

  /**
   * Sends an SMS using the Weloin SMS service.
   * @param {WeloinSmsSimple} sms - SMS object to send.
   * @returns {Promise<Object>} A promise that resolves with the response data from the API.
   * @throws {Error} If there is an error while sending the SMS.
   */
  async sendSms(sms) {
    try {
      const res = await axios.post(
        `${this.baseUrl}/sms`,
        { ...sms },
        {
          headers: { Authorization: `Bearer ${this.accessKey}` },
        }
      )

      return res.data
    } catch (err) {
      console.log('Error', err)
      throw new Error(err)
    }
  }

  /**
   * Sends an OTP using the Weloin SMS service.
   * @param {WeloinSmsOtp} sms - OTP object to send.
   * @returns {Promise<Object>} A promise that resolves with the response data from the API.
   * @throws {Error} If there is an error while sending the OTP.
   */
  async sendOtp(sms) {
    try {
      const res = await axios.post(
        `${this.baseUrl}/otps/send`,
        { ...sms },
        {
          headers: { Authorization: `Bearer ${this.accessKey}` },
        }
      )

      return res.data
    } catch (err) {
      console.log('Error', err)
      throw new Error(err)
    }
  }

  /**
   * Validates an OTP using the Weloin SMS service.
   * @param {Object} payload - Payload containing OTP validation data.
   * @returns {Promise<Object>} A promise that resolves with the response data from the API.
   * @throws {Error} If there is an error while validating the OTP.
   */
  async validateOtp(payload) {
    console.log('Payload ', payload)

    try {
      const res = await axios.post(
        `${this.baseUrl}/otps/validate`,
        { ...payload },
        {
          headers: { Authorization: `Bearer ${this.accessKey}` },
        }
      )

      return res.data
    } catch (err) {
      console.log('Error', err)
      throw new Error(err)
    }
  }
}

module.exports = WeloinSms

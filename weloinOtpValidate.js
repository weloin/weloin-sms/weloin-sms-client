const WeloinSms = require('./weloinSms')

/**
 * WeloinOtpValidate class provides functionality to validate an OTP (One-Time Password) using the Weloin SMS service.
 */
class WeloinOtpValidate {
  /**
   * Creates a new instance of WeloinOtpValidate.
   * @param {Object} options - Options for initializing WeloinOtpValidate.
   * @param {string} options.otp - The OTP to validate.
   * @param {string} options.mobile - The mobile number associated with the OTP.
   * @param {string} options.action - The action associated with the OTP validation.
   */
  static make(...args) {
    return new WeloinOtpValidate(...args)
  }

  constructor({ otp, mobile, action }) {
    this.otp = otp
    this.mobile = mobile
    this.action = action
  }

  async validate() {
    return await WeloinSms.instance().validateOtp(this)
  }
}

module.exports = WeloinOtpValidate

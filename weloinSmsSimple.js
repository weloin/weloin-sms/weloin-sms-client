const WeloinSms = require('./weloinSms')

/**
 * WeloinSmsSimple class represents a simple SMS message to be sent using the Weloin SMS service.
 */
class WeloinSmsSimple {
  static make(...args) {
    return new WeloinSmsSimple(...args)
  }

  /**
   * Initializes a new instance of WeloinSmsSimple.
   * @param {Object} options - Options for initializing WeloinSmsSimple.
   * @param {Array} options.vars - Variables to be used in the SMS message template.
   * @param {string} options.mobile - Mobile number to which the SMS will be sent.
   * @param {string} options.templateId - ID of the SMS message template.
   */
  constructor({ vars, mobile, templateId }) {
    this.vars = vars
    this.mobile = mobile
    this.templateId = templateId
  }

  /**
   * Sends the SMS message using the Weloin SMS service.
   * @returns {Promise<Object>} A promise that resolves with the response data from the API.
   */
  send() {
    return WeloinSms.instance().send(this)
  }
}

module.exports = WeloinSmsSimple

const WeloinSms = require('./weloinSms')

/**
 * WeloinSmsOtp class represents an OTP (One-Time Password) message to be sent using the Weloin SMS service.
 */
class WeloinSmsOtp {
  static make(...args) {
    return new WeloinSmsOtp(...args)
  }

  /**
   * Creates a new instance of WeloinSmsOtp.
   * @param {Object} options - Options for initializing WeloinSmsOtp.
   * @param {Array} options.vars - Variables to be used in the OTP message template.
   * @param {string} options.mobile - Mobile number to which the OTP will be sent.
   * @param {string} options.action - Action associated with the OTP.
   * @param {string} options.templateId - ID of the OTP message template.
   * @param {string} [options.format='num'] - OTP format: 'alpha', 'num', 'alpha-num'.
   * @param {number} [options.length=4] - Length of the OTP.
   * @param {number} [options.varIndexOtp=0] - Index of the OTP in the 'vars' array.
   * @param {number} [options.varIndexTimeout=1] - Index of the OTP timeout in the 'vars' array.
   * @param {number} [options.timeout=600] - Timeout for OTP expiration in seconds.
   */
  constructor({
    vars,
    mobile,
    action,
    templateId,
    format,
    length,
    varIndexOtp,
    varIndexTimeout,
    timeout,
  }) {
    this.vars = vars
    this.mobile = mobile
    this.action = action
    this.templateId = templateId
    this.format = format
    this.length = length
    this.varIndexOtp = varIndexOtp
    this.varIndexTimeout = varIndexTimeout
    this.timeout = timeout
  }

  /**
   * Sends the OTP message using the Weloin SMS service.
   * @returns {Promise<Object>} A promise that resolves with the response data from the API.
   */
  send() {
    return WeloinSms.instance().send(this)
  }
}

module.exports = WeloinSmsOtp

# Weloin SMS Service

The Weloin SMS Service is a Node.js npm package that provides functionality for sending SMS messages and OTPs (One-Time Passwords) using the Weloin SMS service.

## Installation

To install the Weloin SMS Service package, use npm:

```bash
npm install @weloin/sms-client
```

## Usage

### Sending SMS Messages

```javascript
const { WeloinSms, WeloinSmsSimple } = require('@weloin/sms-client')

// Option 1: Directly set the values in the constructor
const weloinSms = new WeloinSms({
  baseUrl: 'https://your-api-url.com', // Replace this with your actual API base URL
  accessKey: 'your-access-key', // Replace this with your actual access key
})

// Option 2: You can put your `baseUrl` and `accessKey` in .env file also and then use this
const weloinSms = WeloinSms.instance()

// Initialize SMS object
const sms = WeloinSmsSimple.make({
  vars: ['var1', 'var2', 'var3'],
  mobile: '1234567890',
  templateId: 'sms_template_id',
})

// Send SMS
sms
  .send()
  .then((response) => console.log('SMS sent successfully:', response))
  .catch((error) => console.error('Error sending SMS:', error))
```

### Sending OTPs

```javascript
const { WeloinSms, WeloinSmsOtp } = require('@weloin/sms-client')

// Option 1: Directly set the values in the constructor
const weloinSms = new WeloinSms({
  baseUrl: 'https://your-api-url.com', // Replace this with your actual API base URL
  accessKey: 'your-access-key', // Replace this with your actual access key
})

// Option 2: You can put your `baseUrl` and `accessKey` in .env file also and then use this
const weloinSms = WeloinSms.instance()

// Initialize OTP object
const otp = WeloinSmsOtp.make({
  vars: ['var1', 'var2', 'var3'],
  mobile: '1234567890',
  action: 'login',
  templateId: 'otp_template_id',
})

// Send OTP
otp
  .send()
  .then((response) => console.log('OTP sent successfully:', response))
  .catch((error) => console.error('Error sending OTP:', error))
```

### Validate OTPs

```javascript
const { WeloinOtpValidate, WeloinSms } = require('@weloin/sms-client')

// Option 1: Directly set the values in the constructor
const weloinSms = new WeloinSms({
  baseUrl: 'https://your-api-url.com', // Replace this with your actual API base URL
  accessKey: 'your-access-key', // Replace this with your actual access key
})

// Option 2: You can put your `baseUrl` and `accessKey` in .env file also and then use this
const weloinSms = WeloinSms.instance()

// Initialize payload object for validate otp
const payload = WeloinOtpValidate.make({
  mobile: '1234567890',
  action: 'login',
  templateId: 'otp_template_id',
})

// Validate OTP
otp
  .validate()
  .then((response) => console.log('OTP validate successfully:', response))
  .catch((error) => console.error('Error sending OTP:', error))
```

## Configuration

Before using the Weloin SMS Service, make sure to set the following environment variables:

- `WELOINSMS_BASE_URL`: Base URL of the Weloin SMS service.
- `WELOINSMS_ACCESS_KEY`: Access key for authentication with the Weloin SMS service.

## API Reference

- [WeloinSmsSimple](#weloinsmssimple-class)
- [WeloinSmsOtp](#weloinsmsotp-class)
- [WeloinSms](#weloinsms-class)

### WeloinSmsSimple Class

Represents a simple SMS message to be sent using the Weloin SMS service.

- **Constructor**: Initializes a new instance of WeloinSmsSimple.
  - `options.vars`: Variables to be used in the SMS message template.
  - `options.mobile`: Mobile number to which the SMS will be sent.
  - `options.templateId`: ID of the SMS message template.

### WeloinSmsOtp Class

Represents an OTP (One-Time Password) message to be sent using the Weloin SMS service.

- **Constructor**: Initializes a new instance of WeloinSmsOtp.
  - `options.vars`: Variables to be used in the OTP message template.
  - `options.mobile`: Mobile number to which the OTP will be sent.
  - `options.action`: Action associated with the OTP.
  - `options.templateId`: ID of the OTP message template.
  - `options.format`: OTP format: 'alpha', 'num', 'alpha-num'. (Optional, default is 'num').
  - `options.length`: Length of the OTP. (Optional, default is 4).
  - `options.timeout`: Timeout for OTP expiration in seconds. (Optional, default is 600 secs).
  - `options.varIndexOtp`: Index of the OTP in the 'vars' array. (Optional, default is 0).
  - `options.varIndexTimeout`: Index of the OTP timeout in the 'vars' array. (Optional, default is 1).

### WeloinSms Class

Provides functionality to send SMS and OTP using the Weloin SMS service.

- **Constructor**: Initializes a new instance of WeloinSms.
  - `options.baseUrl`: Base URL of the Weloin SMS service.
  - `options.accessKey`: Access key for authentication.

### WeloinOtpValidate Class

Provides functionality to validate an OTP(One-Time Password) using the Weloin SMS service.

- **Constructor**: Initializes a new instance of WeloinOtpValidate.
  - `options.otp`: The OTP to validate.
  - `options.mobile`: The mobile number associated with the OTP.
  - `options.action`: The action associated with the OTP.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
